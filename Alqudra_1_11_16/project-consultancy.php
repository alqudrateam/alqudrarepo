<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alqudra</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <!-- Jquery -->
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>


</head>

<body class="default">
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 clearfix">
                <div class="logo">
                    <a href="index.php"><img src="images/logo.jpg" alt="Alqudra Sports Management" /></a>
                </div>
                <div class="header_nav">
                    <ul class="header_navigation clearfix">
                        <li><a href="index.php">home</a></li>
                        <li><a href="about-us.php">about us</a></li>
                        <li><a href="career.php">career</a></li>
                        <li><a href="contact-us.php">contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="subnav_wrapper">
                    <ul class="subnav clearfix">
                        <li><a href="project-consultancy.php" class="active">project consultancy</a></li>
                        <li><a href="man-power-service.php">man power service</a></li>
                        <li><a href="management-service.php">management service</a></li>
                        <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                        <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                        <li><a href="portfolio.php">portfolio</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="#">home</a></li>
                        <li>&#47;</li>
                        <li><a href="#">project consultancy</a></li>
                        <li>&#47;</li>
                        <li><a href="javascript:void(0)" class="active">man power service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container content">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4">
            <div class="sidebar">
                <h4>project consultancy</h4>
                <ul class="sidebar_nav">
                    <li><a href="#" class="active">Design &amp; Layout Analysis</a></li>
                    <li><a href="#">Business Plans &amp; Proposals</a></li>
                    <li><a href="#">Launching Campaigns</a></li>
                    <li><a href="#">Professional Recruitment</a></li>
                    <li><a href="#">Contracts &amp; Licensing</a></li>
                    <li><a href="#">Procurement &amp; IT</a></li>
                    <li><a href="#">Testing &amp; Commissioning</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6"> <!-- inner_content_bg-->
            <div class="main_content">
                <p>Working closely with the stakeholders, AQSM analyses and defines the facility's concept, operational and technical requirements, and investment projections. In addition, complete project management with comprehensive reporting and scheduling of major milestones is carefully planned.</p>
            </div>
            <div class="sub_contents">
                <div class="sub_content_item active">
                    <h3 class="sub_content_head">Design &amp; Layout Analysis</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Create an ambience for sports and recreation</li>
                            <li>Plan efficient and smooth layout</li>
                            <li>Identify and advise on safety norms</li>
                            <li>Liaise with contractors and other consultants</li>
                        </ul>
                    </div>
                </div>
                <div class="sub_content_item">
                    <h3 class="sub_content_head">Business Plans &amp; Proposals</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Market and Competitor Analysis</li>
                            <li>Feasibility studies &amp; Financial Forecasts</li>
                            <li>Technical &amp; Commercial Proposals</li>
                        </ul>
                    </div>
                </div>
                <div class="sub_content_item">
                    <h3 class="sub_content_head">Launching Campaigns</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Pre-launch activities</li>
                            <li>Sale of memberships</li>
                            <li>Advertising &amp; Promoting Campaigns</li>
                        </ul>
                    </div>
                </div>
                <div class="sub_content_item">
                    <h3 class="sub_content_head">Professional Recruitment</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Advertising and headhunting for skilled personnel</li>
                            <li>International and local recruitment activities</li>
                            <li>Interview process (Technical and practical)</li>
                            <li>Public Relations</li>
                            <li>Training &amp; Orientation</li>
                        </ul>
                    </div>
                </div>
                <div class="sub_content_item">
                    <h3 class="sub_content_head">Contracts &amp; Licensing</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Advise on commercial terms for sub-contractor service contracts</li>
                            <li>Liaise with Government Institutes to obtain required licenses to operate</li>
                            <li>Interview process (Technical and practical)</li>
                            <li>Advise and obtain specialized Insurance policies</li>
                        </ul>
                    </div>
                </div>
                <div class="sub_content_item">
                    <h3 class="sub_content_head">Procurement &amp; IT Services</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Design and float RFP for FF&amp;E items</li>
                            <li>Supplier analysis and ratings</li>
                            <li>Comparisons and Negotiations</li>
                            <li>Order, handling, delivery and installations</li>
                            <li>Documentations</li>
                        </ul>
                    </div>
                </div>
                <div class="sub_content_item">
                    <h3 class="sub_content_head">Testing &amp; Commissioning</h3>
                    <div class="sub_content_dyn">
                        <ul>
                            <li>Coordinate with building contractors</li>
                            <li>Conduct and document safety checks</li>
                            <li>Comparisons and Negotiations</li>
                            <li>Facility takeover and handover procedure</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_logo">
                    <a href="#">
                        <img src="images/wht_logo.png" alt="Alqudra Sports Management">
                    </a>
                </div>
                <div class="social">
                    <a href="#" class="fb"><i class="ion ion-social-facebook"></i></a>
                    <a href="#" class="tw"><i class="ion ion-social-twitter"></i></a>
                    <a href="#" class="gp"><i class="ion ion-social-googleplus"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_lnks">
                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li><a href="about-us.php">about us</a></li>
                        <li><a href="career.php">career</a></li>
                        <li><a href="contact-us.php">contact us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_lnks">
                    <ul>
                        <li><a href="project-consultancy.php">project consultancy</a></li>
                        <li><a href="man-power-service.php">man power service</a></li>
                        <li><a href="management-service.php">management service</a></li>
                        <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                        <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                        <li><a href="portfolio.php">portfolio</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="pull-left">&copy; Alqudra SM. All Rights Reserved</div>
            <div class="pull-right">Powered by <a href="http://www.bodhiinfo.com" target="_blank" style="vertical-align: text-bottom;"><img src="images/bodhi_logo.png" alt="Bodhi Info Solutions" /></a></div>
        </div>
    </div>
</div>
</body>
</html>