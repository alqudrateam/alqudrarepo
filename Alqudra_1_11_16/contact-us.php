<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alqudra</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <!-- Jquery -->
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


</head>

<body class="default">
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 clearfix">
                <div class="logo">
                    <a href="index.php"><img src="images/logo.jpg" alt="Alqudra Sports Management"/></a>
                </div>
                <div class="header_nav">
                    <ul class="header_navigation clearfix">
                        <li><a href="index.php">home</a></li>
                        <li><a href="about-us.php">about us</a></li>
                        <li><a href="career.php">career</a></li>
                        <li><a href="contact-us.php" class="active">contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="subnav_wrapper">
                    <ul class="subnav clearfix">
                        <li><a href="project-consultancy.php">project consultancy</a></li>
                        <li><a href="man-power-service.php">man power service</a></li>
                        <li><a href="management-service.php">management service</a></li>
                        <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                        <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                        <li><a href="portfolio.php">portfolio</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li>&#47;</li>
                        <li><a href="javascript:void(0)" class="active">contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Contents -->
<div class="container content">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="address">
                <h4>Address</h4>
                <p>Al Qudra Sports Management LLC<br/>
                    Sheikh Mohammed Bin Zayed Stadium<br/>
                    Al Jazeera Club, Level 1, The Terrace, West Wing<br/>
                    P.O. Box 42612<br/>
                    Abu Dhabi<br/>
                    United Arab Emirates<br/>
                    Tel : <a href="tel:+971-2-4488012">+971-2-4488012</a><br/>
                    Fax : <a href="tel:+971-2-4488013/14/15">+971-2-4488013/14/15</a><br/>
                    Email : <a href="mailto:inquiry@alqudra.ae">inquiry@alqudra.ae</a></p>
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-7">
            <form class="contact">
                <h4>Send us your feedback</h4>
                <div class="form-block">
                    <span><i class="ion ion-person"></i></span>
                    <input type="text" name="name" placeholder="Enter your name here">
                </div>
                <div class="form-block">
                    <span><i class="ion ion-email"></i></span>
                    <input type="email" name="email" placeholder="Enter your email ID here">
                </div>
                <div class="form-block">
                    <textarea name="message" placeholder="Enter your message here"></textarea>
                </div>
            </form>
        </div>
    </div>
    <div class="row" style="margin-top: 50px;">
        <div class="col-lg-12">
            <div id="map"></div>
        </div>
    </div>
</div>


<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_logo">
                    <a href="#">
                        <img src="images/wht_logo.png" alt="Alqudra Sports Management">
                    </a>
                </div>
                <div class="social">
                    <a href="#" class="fb"><i class="ion ion-social-facebook"></i></a>
                    <a href="#" class="tw"><i class="ion ion-social-twitter"></i></a>
                    <a href="#" class="gp"><i class="ion ion-social-googleplus"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_lnks">
                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li><a href="about-us.php">about us</a></li>
                        <li><a href="career.php">career</a></li>
                        <li><a href="contact-us.php">contact us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_lnks">
                    <ul>
                        <li><a href="project-consultancy.php">project consultancy</a></li>
                        <li><a href="man-power-service.php">man power service</a></li>
                        <li><a href="management-service.php">management service</a></li>
                        <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                        <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                        <li><a href="portfolio.php">portfolio</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="pull-left">&copy; Alqudra SM. All Rights Reserved</div>
            <div class="pull-right">Powered by <a href="http://www.bodhiinfo.com" target="_blank"
                                                  style="vertical-align: text-bottom;"><img src="images/bodhi_logo.png"
                                                                                            alt="Bodhi Info Solutions"/></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript">
    window.marker = null;

    function initialize() {
        var map;

        var nottingham = new google.maps.LatLng(24.4531216,54.3911000);

        var style = [
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {"saturation": 50},
                    {"gamma": 1},
                    {"visibility": "on"},
                    {"hue": "#00aced"}
                ]
            },
            {
                "elementType": "geometry", "stylers": [
                {"saturation": -500}
            ]
            }
        ];

        var mapOptions = {
            // SET THE CENTER
            center: nottingham,

            // SET THE MAP STYLE & ZOOM LEVEL
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 16,

            // SET THE BACKGROUND COLOUR
            backgroundColor: "#eeeeee",

            // REMOVE ALL THE CONTROLS EXCEPT ZOOM
            scrollwheel: false,
            panControl: false,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: false,
            DoubleClickZoom: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            }

        }
        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        // SET THE MAP TYPE
        var mapType = new google.maps.StyledMapType(style, {name: "Grayscale"});
        map.mapTypes.set('grey', mapType);
        map.setMapTypeId('grey');

        //CREATE A CUSTOM PIN ICON
        var marker_image = 'images/pin.png';
        var pinIcon = new google.maps.MarkerImage(marker_image, null, null, null, new google.maps.Size(51, 74));

        marker = new google.maps.Marker({
            position: nottingham,
            map: map,
            icon: pinIcon,
            title: 'Absolute Nottingham'
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
</body>
</html>