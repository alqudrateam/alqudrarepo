/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function () {
    "use strict";
    // Toggling the sub contents on side panel
    $(document).on('click', '.sidebar_nav > li > a', function (e) {
        e.preventDefault();
        $('.sidebar_nav > li a').not($(this)).removeClass('active');
        $(this).addClass('active');
        var lnkIndex = $(this).parent().index() + 1;
        $('.sub_contents .sub_content_item').not($('.sub_contents .sub_contents_item:nth-child(' + lnkIndex + ')')).removeClass('active');
        $('.sub_contents .sub_content_item:nth-child(' + lnkIndex + ')').addClass('active');
    });
});