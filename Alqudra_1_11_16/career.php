<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alqudra</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <!-- Jquery -->
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


</head>

<body class="default">
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 clearfix">
                <div class="logo">
                    <a href="index.php"><img src="images/logo.jpg" alt="Alqudra Sports Management" /></a>
                </div>
                <div class="header_nav">
                    <ul class="header_navigation clearfix">
                        <li><a href="index.php">home</a></li>
                        <li><a href="about-us.php">about us</a></li>
                        <li><a href="career.php" class="active">career</a></li>
                        <li><a href="contact-us.php">contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="subnav_wrapper">
                    <ul class="subnav clearfix">
                        <li><a href="project-consultancy.php">project consultancy</a></li>
                        <li><a href="man-power-service.php">man power service</a></li>
                        <li><a href="management-service.php">management service</a></li>
                        <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                        <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                        <li><a href="portfolio.php">portfolio</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li>&#47;</li>
                        <li><a href="javascript:void(0)" class="active">career</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Contents -->
<div class="container content">
    <p> At AQSM, we believe our strength is in our people and we go beyond expectations to provide a work environment where you can reach your full potential. A career at AQSM means an opportunity to explore, develop, grow and fulfill your ambition.<br />Join a company that values you At the core of our people strategy is our focus on behaviors that brings out the very best from every employee. We assess performance not just on results but on how those results are achieved.<br />Diversity We employ from diverse backgrounds, and our workforce is composed of people from various countries across the globe. </p>
    <h4 class="text-capitalize">employee benefits</h4>
    <ul class="benefits">
        <li>Competitive Packages: Every package offered includes Tax Free Basic Salary, an accommodation allowance, and a transportation allowance. A revenue sharing scheme is applicable depending on the job description and is paid monthly.</li>
        <li>Annual Leave: We provide 30 calendar days of annual leave as well as all public holidays.</li>
        <li>Leave Tickets: Free annual or bi-annual leave tickets are provided to the employee.</li>
        <li>Medical insurance: The company provides a comprehensive medical insurance to its employees.</li>
    </ul>
</div>


<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_logo">
                    <a href="#">
                        <img src="images/wht_logo.png" alt="Alqudra Sports Management">
                    </a>
                </div>
                <div class="social">
                    <a href="#" class="fb"><i class="ion ion-social-facebook"></i></a>
                    <a href="#" class="tw"><i class="ion ion-social-twitter"></i></a>
                    <a href="#" class="gp"><i class="ion ion-social-googleplus"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_lnks">
                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li><a href="about-us.php">about us</a></li>
                        <li><a href="career.php">career</a></li>
                        <li><a href="contact-us.php">contact us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_lnks">
                    <ul>
                        <li><a href="project-consultancy.php">project consultancy</a></li>
                        <li><a href="man-power-service.php">man power service</a></li>
                        <li><a href="management-service.php">management service</a></li>
                        <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                        <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                        <li><a href="portfolio.php">portfolio</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="pull-left">&copy; Alqudra SM. All Rights Reserved</div>
            <div class="pull-right">Powered by <a href="http://www.bodhiinfo.com" target="_blank" style="vertical-align: text-bottom;"><img src="images/bodhi_logo.png" alt="Bodhi Info Solutions" /></a></div>
        </div>
    </div>
</div>
</body>
</html>