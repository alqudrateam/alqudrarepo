-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2016 at 09:51 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alqudra`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `category`) VALUES
(1, 'Project Consultancy'),
(2, 'Manpower & Management Service'),
(3, 'Civil & Interior Decor');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`ID`, `content`) VALUES
(1, 'Project consultancy'),
(2, 'Man power service'),
(3, 'Management service'),
(4, 'Civil & interior decor'),
(5, 'Brands & franchises');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `userName`, `password`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) NOT NULL,
  `workName` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `logo` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`ID`, `categoryId`, `workName`, `description`, `image`, `logo`) VALUES
(12, 1, 'Wc11', 'wss11', 'image/20160806004242.png', 'logo/20160806004242.png'),
(13, 2, 'sdfsfde', 'fghfg', 'image/20160806005951.jpg', 'logo/20160806005951.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `contentId` int(11) NOT NULL,
  `service` text NOT NULL,
  `english` text NOT NULL,
  `arabic` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`ID`, `contentId`, `service`, `english`, `arabic`) VALUES
(1, 1, 'Design & Layout Analysis', 'Create an ambience for sports and recreation\r\nPlan efficient and smooth layout\r\nIdentify and advise on safety norms\r\nLiaise with contractors and other consultants', ''),
(2, 1, 'Business Plans & Proposals', 'Market and Competitor Analysis\r\nFeasibility studies & Financial Forecasts\r\nTechnical & Commercial Proposals', ''),
(3, 1, 'Launching Campaigns', 'Pre-launch activities\r\nSale of memberships\r\nAdvertising & Promoting Campaigns', ''),
(4, 1, 'Professional Recruitment', 'Advertising and headhunting for skilled personnel\r\nInternational and local recruitment activities\r\nInterview process (Technical and practical)\r\nPublic Relations\r\nTraining & Orientation', ''),
(5, 1, 'Contracts & Licensing', 'Advise on commercial terms for sub-contractor service contracts\r\nLiaise with Government Institutes to obtain required licenses to operate\r\nInterview process (Technical and practical)\r\nAdvise and obtain specialized Insurance policies', ''),
(6, 1, 'Procurement & It Services', 'Design and float RFP for FF&E items\r\nSupplier analysis and ratings\r\nComparisons and Negotiations\r\nOrder, handling, delivery and installations\r\nDocumentations', ''),
(7, 1, 'Testing & Commissioning', 'Coordinate with building contractors\r\nConduct and document safety checks\r\nComparisons and Negotiations\r\nFacility takeover and handover procedure', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
