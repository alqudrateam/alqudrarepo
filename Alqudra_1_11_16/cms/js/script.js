/*jslint browser: true*/
/*global $, jQuery, alert*/
$(window).load(function () {
    'use strict';
    // Session Message
    $('#session_modal').modal('show');
});
$(function () {
    'use strict';
    // Global functions
    // Error message
    function triggerError(type, msg) {
        if (type == "error") {
            $('#error_pop .modal-body p').text(msg);
        }
        $('#error_pop').modal('show');
    }
    // Date picker
    if ($('.user_date').length > 0) {
        $('.user_date').each(function () {
            var curDatePicker = $(this);
            //alterTarget = curDatePicker.nextAll('.date_hidden');
            curDatePicker.datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
            });
        });
    }

    $('.header_main_nav > li.has_child > a').click(function (e) {
        e.preventDefault();
        var targetDropMenu = $(this).next();
        $('.header_sub_nav').not(targetDropMenu).hide();
        targetDropMenu.toggle();
    });
    $('ul.side_nav > li.has_child > a').click(function (e) {
        e.preventDefault();
        var curSideNav = $(this),
            curTargetSubNav = curSideNav.nextAll('ul.side_sub_nav');
        $('ul.side_sub_nav').not(curTargetSubNav).slideUp();
        curTargetSubNav.slideToggle();
    });
    $('.side_panel').perfectScrollbar();
    $('.nav_toggler').click(function () {
        $('.side_panel').toggleClass('opened');
        $('.wrapper').toggleClass('give_way');
    });
    // Login form
    $('.login_form_block_inner input').focusin(function () {
        $(this).addClass('focus');
        $(this).prev().addClass('focus');
    });
    $('.login_form_block_inner input').focusout(function () {
        $(this).removeClass('focus');
        $(this).prev().removeClass('focus');
    });



});