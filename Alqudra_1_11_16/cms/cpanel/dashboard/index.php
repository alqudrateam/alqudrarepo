<?php
require('../admin_header.php');

if($_SESSION['LogID'] != 2){
    header("location:../../logout.php");
} else {
    if (@isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
    }
    unset($_SESSION['msg']);
?>
    <h3>Dashboard</h3>
    <div class="e">
        <span><i class="ion ion-load-d"></i></span>
    </div>
<?php
    require('../admin_footer2.php');
}