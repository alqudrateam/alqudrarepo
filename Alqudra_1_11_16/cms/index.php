<?php session_start();?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/perfect-scrollbar.jquery.js"></script>
    <script src="js/script.js"></script>
</head>

<body>
<div class="login_wrapper">
    <div class="login_inner">
        <div class="login_head">
            <h3>Login</h3>
        </div>
        <form action="do.php" method="post">
            <div class="login_form_block">
                <div class="login_form_block_inner">
                    <span class="login_form_ico"><i class="ion ion-android-person"></i></span>
                    <input type="text" name="userName" id="userName" placeholder="User name">
                </div>
            </div>
            <div class="login_form_block">
                <div class="login_form_block_inner">
                    <span class="login_form_ico"><i class="ion ion-key"></i></span>
                    <input type="password" name="password" id="password" placeholder="Password"/>
                </div>
            </div>
            <div class="login_form_block">
                <div class="login_form_block_inner">
                    <input type="submit" name="ok" value="login"/>
                </div>
            </div>
        </form>
    </div>
</div>
<!--<form action="do.php" method="post">
	<input type="text" name="userName" id="userName"/><br>
	<input type="password" name="password" id="password"/><br>
	<input type="submit" name="ok" value="login"/>
</form>-->
<?php

if (@isset($_SESSION['msg'])) {
    echo $_SESSION['msg'];
}
unset($_SESSION['msg']);
?>
</body>

</html>