<!DOCTYPE HTML>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alqudra</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <!-- Jquery -->
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>


</head>

<body class="default">
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 clearfix">
                    <div class="logo">
                        <a href="index.php"><img src="images/logo.jpg" alt="Alqudra Sports Management" /></a>
                    </div>
                    <div class="header_nav">
                        <ul class="header_navigation clearfix">
                            <li><a href="index.php">home</a></li>
                            <li><a href="about-us.php">about us</a></li>
                            <li><a href="career.php">career</a></li>
                            <li><a href="contact-us.php">contact us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="subnav_wrapper">
                        <ul class="subnav clearfix">
                            <li><a href="project-consultancy.php">project consultancy</a></li>
                            <li><a href="man-power-service.php">man power service</a></li>
                            <li><a href="management-service.php">management service</a></li>
                            <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                            <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                            <li><a href="portfolio.php" class="active">portfolio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="#">home</a></li>
                            <li>&#47;</li>
                            <li><a href="#">portfolio</a></li>
                            <li>&#47;</li>
                            <li><a href="javascript:void(0)" class="active">research and planning</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container content">
        <div class="row" style="    padding-bottom: 15px;">
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="sidebar">
                    <h4>portfolio</h4>
                    <ul class="sidebar_nav">
                        <li><a href="#" class="active">Project Consultancy</a></li>
                        <li><a href="#">Manpower &amp; Management Service</a></li>
                        <li><a href="#">Civil &amp; Interior Décor</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 inner_content_bg">
                <!-- <div class="main_content">
                <p>Working closely with the stakeholders, AQSM analyses and defines the facility's concept, operational and technical requirements, and investment projections. In addition, complete project management with comprehensive reporting and scheduling of major milestones is carefully planned.</p>
            </div>-->
                <div class="sub_contents">
                    <div class="sub_content_item active portfolio_row">
                        <h3 class="sub_content_head">Project Consultancy</h3>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/Abu-Dhabi-SPorts-Council.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Abu Dhabi Ladies Club (ADLC)</h4></li>
                                    <p>The AQSM was appointed as the operational consultant by the project owner, Abu Dhabi Sports Council (ADSC) to provide with business plans and feasibility studies. The AQSM was coordinating with the design consultants to finalize the layout analysis of 13,500 sqm project.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/3P95_nagy.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="sub_content_item  portfolio_row">
                        <h3 class="sub_content_head">Manpower &amp; Management Service</h3>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/GHQ.png" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>General Headquarters (GHQ)</h4></li>
                                    <p>The GHQ has been a longstanding client of the AQSM since 2002. The AQSM operates over 30 sports facilities for the GHQ in all Emirates and cities.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/000_1218.jpg" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/CICPA.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Critical Infrastructure &amp; Coastal Protection Authority (CICPA)</h4></li>
                                    <p>The CICPA has been obtaining the services from the AQSM for the manpower supply during the past 5 years. The AQSM as part of its contract provides</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/000_0141.jpg" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/PG-Logo.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Presidential Guard</h4></li>
                                    <p>The PG is provided with the management and the manpower services for 5 clubs including an exclusive ladies club.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/000_1084.JPG" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/GHQ.png" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>National Services</h4></li>
                                    <p>The GHQ is supplied with highly skilled professional level 131 numbers of trainers dedicated to the major facilities of the GHQ.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/20140820_105417.jpg" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/Abu-Dhabi-SPorts-Council.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Al Wagan Sports &amp; Recreation Club</h4></li>
                                    <p>An exclusively Men’s facility located in Al Wagan (75 kms from Al Ain). The facility comprises of swimming pool, gym, studio, multipurpose court, sauna, café, bowling, outdoor field and recreation rooms.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/AL_WAGAN_20130325-20.jpg" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/Abu-Dhabi-SPorts-Council.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Delma Island Ladies Club</h4></li>
                                    <p>An exclusive ladies club, located off the coast of the western region of Abu Dhabi. The facility comprises of swimming pool, gym, studio, outdoor field, sauna, steam, café and recreation rooms.
                                        <br> (Both the Al Wagan and the Delma Clubs has been under the AQSM operations since its inception in 2008).</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/DSC05298.JPG" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/ADEC.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Abu Dhabi Education Council (ADEC)</h4></li>
                                    <p>The AQSM has since 2015 has entered into a contract with the ADEC to provide 72 male and female swimming instructors to 36 schools located in Abu Dhabi, Al Ain and Al Gharbia regions.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/ADEC0028-copy.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="sub_content_item portfolio_row">
                        <h3 class="sub_content_head">Civil &amp; Interior Decoration</h3>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/Al-Ain-Sports-Club.png" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Al Ain Sports &amp; Cultural Club</h4></li>
                                    <p>The AQSM renovated the Al Ain Football Club’s first football team’s gym, changing, SPA and the physical treatment areas.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/3P95_nagy.jpg" alt="">
                            </div>
                        </div>
                        <div class="sub_content_dyn">
                            <img src="images/managed_facilities/Baniyas.jpg" alt="">
                            <div class="port_description">
                                <ul>
                                    <li>
                                        <h4>Baniyas Sports &amp; Cultural Center</h4></li>
                                    <p>The AQSM renovated the Baniyas Football Club’s first football team’s gym, changing and the physical treatment areas.</p>
                                </ul>
                            </div>
                            <div class="project_img">
                                <img src="images/3P95_nagy.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_logo">
                        <a href="#">
                            <img src="images/wht_logo.png" alt="Alqudra Sports Management">
                        </a>
                    </div>
                    <div class="social">
                        <a href="#" class="fb"><i class="ion ion-social-facebook"></i></a>
                        <a href="#" class="tw"><i class="ion ion-social-twitter"></i></a>
                        <a href="#" class="gp"><i class="ion ion-social-googleplus"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_lnks">
                        <ul>
                            <li><a href="index.php">home</a></li>
                            <li><a href="about-us.php">about us</a></li>
                            <li><a href="career.php">career</a></li>
                            <li><a href="contact-us.php">contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_lnks">
                        <ul>
                            <li><a href="project-consultancy.php">project consultancy</a></li>
                            <li><a href="man-power-service.php">man power service</a></li>
                            <li><a href="management-service.php">management service</a></li>
                            <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                            <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                            <li><a href="portfolio.php">portfolio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="pull-left">&copy; Alqudra SM. All Rights Reserved</div>
                <div class="pull-right">Powered by
                    <a href="http://www.bodhiinfo.com" target="_blank" style="vertical-align: text-bottom;"><img src="images/bodhi_logo.png" alt="Bodhi Info Solutions" /></a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>