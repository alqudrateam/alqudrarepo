<!DOCTYPE HTML>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alqudra</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


    <!-- Jquery -->
    <script src="js/jquery-2.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.flexslider.js"></script>


</head>

<body class="default">
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 clearfix">
                    <div class="logo">
                        <a href="index.php"><img src="images/logo.jpg" alt="Alqudra Sports Management" /></a>
                    </div>
                    <div class="menu_wrap">
                        <div class="header_nav">
                            <ul class="header_navigation clearfix">
                                <li><a href="index.php" class="active">home</a></li>
                                <li><a href="about-us.php">about us</a></li>
                                <li><a href="career.php">career</a></li>
                                <li><a href="contact-us.php">contact us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="subnav_wrapper">
                        <ul class="subnav clearfix">
                            <li><a href="project-consultancy.php">project consultancy</a></li>
                            <li><a href="man-power-service.php">man power service</a></li>
                            <li><a href="management-service.php">management service</a></li>
                            <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                            <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                            <li><a href="portfolio.php">portfolio</a></li>
                        </ul>
                    </div>
                    <div id="myNav" class="overlay_side">
                      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
                        <ul class="overlay-content">
                            <li><a href="project-consultancy.php">project consultancy</a></li>
                            <li><a href="man-power-service.php">man power service</a></li>
                            <li><a href="management-service.php">management service</a></li>
                            <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                            <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                            <li><a href="portfolio.php">portfolio</a></li>
                        </ul>
                    </div>
                    <div class="nav_draw_icon">
                        <span class="menu_trigger" style="font-size:30px;cursor:pointer;"  onclick="openNav()">☰</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="javascript:void(0)" class="active">home</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animatio">
       <style type="text/css" media="screen">
		#flashContent { width:100%; height:100%; }
		</style>
		<div id="flashContent">
			<center><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1135" height="400" id="menu" align="middle">
				<param name="movie" value="images/banner.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#ffffff" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="window" />
				<param name="scale" value="showall" />
				<param name="menu" value="true" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="images/banner.swf" width="1135" height="400">
					<param name="movie" value="menu.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#ffffff" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
					<param name="wmode" value="window" />
					<param name="scale" value="showall" />
					<param name="menu" value="true" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object></center>
		</div>
    </div>
    <div class="content_wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="managed_facilities">
                        <h3>Managed Facilities</h3>
                        <hr>

                        <div id="main" role="main">
                            <section class="slider">
                                <div class="flexslider carousel">
                                    <ul class="slides">
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/Abu-Dhabi-SPorts-Council.jpg" alt="Abu Dhabi SPorts Council" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/ADEC.jpg" alt="Abu Dhabi Education Council" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/Al-Ain-Sports-Club.png" alt="Al Ain Sports & Cultural Club" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/Baniyas.jpg" alt="Baniyas Sports & Cultural Center" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/CICPA.jpg" alt="Critical Infrastructure & Coastal Protection Authority" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/GHQ.png" alt="General Headquarters" /></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="images/managed_facilities/PG-Logo.jpg" alt="Presidential Guard" /></a>
                                        </li>

                                    </ul>
                                </div>
                            </section>
                        </div>


                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="powered_by">
                        <h3>Powered by</h3>
                        <hr>
                        <img src="images/AQH.jpg" />
                        <div style="margin-top:15px;"></div>
                        <img src="images/DIRC-logo.jpg" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="brands">
                        <h3>Brands</h3>
                        <hr>
                        <img src="images/World-Gym-Logo.jpg" />
                        <div style="margin-top:25px;"></div>
                        <p>Redefining the essence of an invicoratirg workout environment.</p>
                        <h4>Al Qudra World Gym</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_logo">
                        <a href="#">
                            <img src="images/wht_logo.png" alt="Alqudra Sports Management">
                        </a>
                    </div>
                    <div class="social">
                        <a href="#" class="fb"><i class="ion ion-social-facebook"></i></a>
                        <a href="#" class="tw"><i class="ion ion-social-twitter"></i></a>
                        <a href="#" class="gp"><i class="ion ion-social-googleplus"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_lnks">
                        <ul>
                            <li><a href="index.php">home</a></li>
                            <li><a href="about-us.php">about us</a></li>
                            <li><a href="career.php">career</a></li>
                            <li><a href="contact-us.php">contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_lnks">
                        <ul>
                            <li><a href="project-consultancy.php">project consultancy</a></li>
                            <li><a href="man-power-service.php">man power service</a></li>
                            <li><a href="management-service.php">management service</a></li>
                            <li><a href="civil-interior-decor.php">civil &amp; interior decor</a></li>
                            <li><a href="brands-franchises.php">brands &amp; franchises</a></li>
                            <li><a href="portfolio.php">portfolio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="pull-left">&copy; Alqudra SM. All Rights Reserved</div>
                <div class="pull-right">Powered by
                    <a href="http://www.bodhiinfo.com" target="_blank" style="vertical-align: text-bottom;"><img src="images/bodhi_logo.png" alt="Bodhi Info Solutions" /></a>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: true,
                itemWidth: 173,
                itemMargin: 2,
                atuoPlay: true,
                controlNav: false,
            });
        });
        function openNav() {
                    document.getElementById("myNav").style.width = "100%";
                }

                function closeNav() {
                    document.getElementById("myNav").style.width = "0%";
                }
    </script>
</body>

</html>